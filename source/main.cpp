#include <algorithm>
#include <iostream>
#include <string>
#include <vector>

class Solution {
 public:
  std::string longestCommonPrefix(std::vector<std::string>& strs) {
    if (0 == strs.size())
      return "";
    if (1 == strs.size())
      return strs[0];

    std::vector<int> InputLens(strs.size());
    for (size_t i = 0; i < strs.size(); i++) {
      InputLens[i] = strs[i].length();
    }
    std::sort(InputLens.begin(), InputLens.end());
    if (0 == InputLens[0])
      return "";

    size_t nIdx = 0;
    for (size_t i = 0; i < InputLens[0]; i++) {
      char cChar = strs[0][i];
      for (size_t j = 0; j < strs.size(); j++) {
        if (cChar == strs[j][i])
          nIdx = i;
        else
          return strs[0].substr(0, nIdx);
      }
    }

    return strs[0].substr(0, nIdx + 1);
  }
};

int main() {
  Solution sol;

  std::vector<std::string> Input = {"flower", "flow", "flight"};
  std::string Ret = sol.longestCommonPrefix(Input);
  if (Ret == "fl") {
    std::cout << Ret << std::endl;
    return 0;
  }

  return -1;
}