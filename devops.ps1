#Requires -Version 7.1

using module ./script/pslib.psm1

if ($args.Count -gt 0) {
    #Write-Host "Try to dispatch to a function ..."
    $RunCmd = [RunCmd]::new()
    $RetCode = 0
    $ProjectDir = Resolve-Path $PSScriptRoot
    $SourceDir  = Resolve-Path $PSScriptRoot/source
    switch($args[0]) {
      "Build"     { $RetCode = $RunCmd.RunBuild($ProjectDir)    }
      "LintFmt"   { $RetCode = $RunCmd.RunLintFmt($SourceDir)   }
      "LintTidy"  { $RetCode = $RunCmd.RunLintTidy($SourceDir)  }
    }
} else {
    Write-Host "Please input a command (Build/LintFmt/LintTidy/Clean)."
}
