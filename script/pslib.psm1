#requires -Version 7.1

class PSLib {

    [int] Exec ([string]$Cmd, [string[]]$ArgList, [bool]$Message) {
        $pinfo = New-Object System.Diagnostics.ProcessStartInfo
        $pinfo.FileName               = $Cmd
        if (-Not $Message) {
            $pinfo.RedirectStandardError  = $true
            $pinfo.RedirectStandardOutput = $true
        }
        $pinfo.UseShellExecute        = $false
        $pinfo.Arguments              = $ArgList
        $p = New-Object System.Diagnostics.Process
        $p.StartInfo = $pinfo
        if ($Message) {
            $p.Start()
        } else {
            $p.Start() | Out-Null
        }
        $p.WaitForExit()
        return $p.ErrorCode
    }


    [string[]] ExploreDir([string]$RootDir, [string[]]$ExtNameList, [bool]$Recursive) {
        if (-Not ([IO.Directory]::Exists($RootDir))) {
            [string[]] $EmptyArray = @()
            return $EmptyArray
        }

        if ($Recursive) {
            $FoundList = Get-ChildItem -Path $RootDir -ErrorAction SilentlyContinue -Recurse
        } else {
            $FoundList = Get-ChildItem -Path $RootDir -ErrorAction SilentlyContinue
        }

        if ($ExtNameList) {
            $FoundList = $FoundList | Where-Object {$_.extension -in $ExtNameList} | ForEach-Object{$_.FullName}
        }

        return $FoundList
    }
    
    [int] RunClangFormat([string[]]$FileList) {    
        foreach ($item in $FileList) {
            $ArgList = @("-i" , $item)
            $this.Exec("clang-format", $ArgList, $false)
        }
        return $LastExitCode
    }
    
    [int] RunClangTidy([string[]]$FileList) {    
        foreach ($item in $FileList) {
            $ArgList = @($item)
            $this.Exec("clang-tidy", $ArgList, $true)
        }
        return $LastExitCode
    }
}


class RunCmd {
    $private:PSLib = $null
    
    RunCmd() {
        $this.PSLib = [PSLib]::new()
    }
    
    [int] RunBuild ([string]$ProjectDir) {        
        $ArgList = @($ProjectDir, "-G", "`"Visual Studio 16 2019`"" , "-B", "build")        
        $this.PSLib.Exec("cmake", $ArgList, $true)
        return $LastExitCode
    }
    
    
    [int] RunLintFmt([string]$SourceDir) {    
        [string[]]$ExtNames = '.c', '.cpp', '.cxx'
        $FilePathes = $this.PSLib.ExploreDir($SourceDir, $ExtNames, $true)
        foreach ($Path in $FilePathes) {
            $Ret = $this.PSLib.RunClangFormat($Path)
            Write-Host ([string]::Format("[Ret={0}] {1}", $Ret, $Path))
        }
        return $FilePathes.Count # How many files are found
    }
    
    [int] RunLintTidy([string]$SourceDir) {    
        [string[]]$ExtNames = '.c', '.cpp', '.cxx'
        $FilePathes = $this.PSLib.ExploreDir($SourceDir, $ExtNames, $true)
        foreach ($Path in $FilePathes) {
            $Ret = $this.PSLib.RunClangTidy($Path)
            #Write-Host ([string]::Format("[Ret={0}] {1}", $Ret, $Path))
        }
        return $FilePathes.Count # How many files are found
    }
}